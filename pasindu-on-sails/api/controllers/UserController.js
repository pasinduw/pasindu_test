/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * CommentController.index()
   */
  index: function (req, res) {
    var params = req.params.all()

    User.find({name: params.name}).exec(function afterIndex(err,found) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not show the database entry: ' + err
          });
      }

      return res.json(found);
    });
  },

  /**
   * CommentController.create()
   */
  create: function (req, res) {
    var params = req.params.all()

    User.create({name: params.name, email: params.email, password: params.password}).exec(function afterCreate(err,created) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not create the database entry: ' + err
          });
      }

      return res.json({
        notice: 'Created user with name ' + created.name
      });
    });
  },

   /**
   * CommentController.show()
   */
  show: function (req, res) {
    var params = req.params.all()

    User.find({}).exec(function afterShow(err,found) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not show the records in the database: ' + err
          });
      }

      return res.json(found);
    });
  },

  /**
   * CommentController.edit()
   */
  edit: function (req, res) {
    var params = req.params.all()

    User.update({name: params.name}, {email:params.email, password:params.password}).exec(function afterEdit(err,found) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not update the database entry: ' + err
          });
      }

      return res.json(found);
    });
  },

  /**
   * CommentController.delete()
   */
  delete: function (req, res) {
    var params = req.params.all()

    User.destroy({name: params.name}).exec(function afterDelete(err,found) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not delete the database entry: ' + err
          });
      }

      return res.json(found);
    });
  }

};

