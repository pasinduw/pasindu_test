/**
 * CheckInController
 *
 * @description :: Server-side logic for managing Checkins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

   /**
   * CommentController.show()
   */
  show: function (req, res) {
    var params = req.params.all()

    CheckIn.find({}).exec(function afterShow(err,found) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not show the records in the database: ' + err
          });
      }

      return res.json(found);
    });
  },


  /**
   * CommentController.createMobileOrder()
   */
  createMobileOrder: function (req, res) {
    var params = req.params.all()

    CheckIn.create({checkInType: "MobileNum", 
                    userName: params.userName, 
                   //businessID: params.businessID, 
                   mobileNum: params.mobileNum, 
                   state: params.state, 
                   timeOfLastState: new Date().toISOString(),
                   timeOfCurrentState: new Date().toISOString(),
                   buzzedCount: 0
                  }).exec(function afterCreateMobileOrder(err,created) {

      if (err) {
        console.log(err);

        return res.json({
          notice: 'Could not create the database entry: ' + err
          });
      }

      return res.json({
        notice: 'Created checked in user with number ' + created.mobileNum
      });
    });
  },

};

