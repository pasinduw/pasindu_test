/**
* CheckIn.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    checkInType:{
      type: 'string',
      enum: ['SmartPhone', 'MobileNum'],
      required: true
    },

    userID:{
      type: 'objectID',
      unique: true
    },

    userName:{
      type: 'string',
      required: true
    },

    businessID:{
      type: 'objectID',
      unique: true
    },

    deviceToken:{
      type: 'string',
      unique: true
    },

    mobileNum:{
      type: 'integer',
      unique: true
    },

    state:{
      type: 'string',
      enum: ['CheckedIn', 'WaitingForTable', 'BuzzedForTable', 'WaitingForOrder', 'BuzzedForOrder'],
      required: true
    },

    timeOfLastState:{
      type: 'date',
      required: true
    },

    timeOfCurrentState:{
      type: 'date',
      required: true
    },

    buzzedCount:{
      type: 'integer',
      required: true
    }

  }
};

